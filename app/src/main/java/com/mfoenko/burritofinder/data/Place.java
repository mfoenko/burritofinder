package com.mfoenko.burritofinder.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {

    private String id;
    private String name;
    private String address;
    private float rating;
    private int price;
    private float longitude;
    private float latitude;

    public Place(String id, String name, String address, float rating, int price,  float latitude, float longitude) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.rating = rating;
        this.price = price;
        this.longitude = longitude;
        this.latitude = latitude;
    }
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getRating() {
        return rating;
    }

    public int getPrice(){
        return price;
    }

    public String getAddress(){
        return address;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public String getPriceText(){
        StringBuilder builder = new StringBuilder(3);
        for(int i=0;i<this.price;i++){
            builder.append("$");
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return id+" "+name+" "+latitude+","+longitude;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeFloat(this.rating);
        dest.writeInt(this.price);
        dest.writeFloat(this.longitude);
        dest.writeFloat(this.latitude);
    }

    protected Place(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.address = in.readString();
        this.rating = in.readFloat();
        this.price = in.readInt();
        this.longitude = in.readFloat();
        this.latitude = in.readFloat();
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
