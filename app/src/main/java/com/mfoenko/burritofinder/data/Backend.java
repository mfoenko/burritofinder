package com.mfoenko.burritofinder.data;

import android.location.Location;
import android.util.Log;

import com.mfoenko.burritofinder.BuildConfig;

import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public final class Backend {

    private Backend(){}

    public static final String MAPS_API_KEY = BuildConfig.mapsApiKey;

    private static final OkHttpClient http = new OkHttpClient.Builder().build();

    public static void getPlaces(Location loc, Callback cb){
        if(loc == null){
            return;
        }
        Log.i("BurritoFinderLocation", loc.toString());
        HttpUrl url = new HttpUrl.Builder().scheme("https").host("maps.googleapis.com").addPathSegments("maps/api/place/nearbysearch/json")
                .addQueryParameter("key", MAPS_API_KEY)
                .addQueryParameter("keyword", "Burritos")
                .addQueryParameter("location", loc.getLatitude()+","+loc.getLongitude())//maps api formatting
                .addQueryParameter("rankby","distance")
                .build();
        Request request = new Request.Builder()
                .get().url(url).build();

        http.newCall(request).enqueue(cb);
    }

}
