package com.mfoenko.burritofinder.screen;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mfoenko.burritofinder.R;
import com.mfoenko.burritofinder.data.Backend;
import com.mfoenko.burritofinder.data.Place;

import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = AppCompatActivity.class.getName();

    private FusedLocationProviderClient mLocationProvider;
    private static final int REQUEST_PERMISSION_LOCATION = 12;
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if(locationResult == null) return;
            if(locationResult.getLocations().size() == 0) return;
            updateLocation(locationResult.getLocations().get(locationResult.getLocations().size() - 1), false);
        }
    };
    private Timer mTimer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        mLocationProvider = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if(!checkLocationPermissions()) return;
        mLocationProvider.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                updateLocation(task.getResult(), true);
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void enableCurrentLocation() {
        if(!checkLocationPermissions()) return;
        mLocationProvider.requestLocationUpdates(LocationRequest.create(), mLocationCallback, null);
    }


    private void setPlaces(Place[] places) {
        Log.i(TAG, Arrays.toString(places));
        mPlaces = places;
        mAdapter.notifyDataSetChanged();

        updateEmptyView();
    }

    private void updateEmptyView() {
        findViewById(R.id.empty_load).setVisibility(mPlaces.length != 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.empty_text).setVisibility(mPlaces.length != 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.recyclerview).setVisibility(mPlaces.length != 0 ? View.VISIBLE : View.GONE);
    }

    private boolean checkLocationPermissions() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableCurrentLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationProvider.removeLocationUpdates(mLocationCallback);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case REQUEST_PERMISSION_LOCATION:
                if(grantResults.length == 0) {
                    //ToDo request cancelled
                    return;
                }
                if(grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
                    enableCurrentLocation();
                } else {
                    //ToDo location permission not granted
                }
        }
    }

    private void updateLocation(final Location loc, final boolean isTemp) {
        Backend.getPlaces(loc, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "error in updateLocation", e);
                retryUpdateLocation(loc, isTemp);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i(TAG, "updateLocation: " + response.code());
                if(response.code() == 200) {
                    String body = response.body().string();
                    Log.i(TAG, "updateLocation: " + body);
                    JsonArray placesJson = Json.parse(body).asObject().get("results").asArray();
                    final Place[] places = new Place[placesJson.size()];
                    for(int i = 0; i < placesJson.size(); i++) {
                        JsonObject obj = placesJson.get(i).asObject();
                        places[i] = new Place(
                                obj.getString("id", ""),
                                obj.getString("name", ""),
                                obj.getString("vicinity", ""),
                                obj.getFloat("rating", 0),
                                obj.getInt("price_level", 0),
                                obj.get("geometry").asObject().get("location").asObject().getFloat("lat", 0),
                                obj.get("geometry").asObject().get("location").asObject().getFloat("lng", 0)
                        );
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setPlaces(places);
                        }
                    });
                }else{
                    retryUpdateLocation(loc, isTemp);
                }
            }
        });
    }

    private void retryUpdateLocation(final Location loc, final boolean isTemp) {
        //ToDo some more intelligent retrying including quiting on old attempts (after location gets updated)
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                updateLocation(loc, isTemp);
            }
        },1500);
    }

    Place[] mPlaces = new Place[0];
    private RecyclerView.Adapter<PlaceViewHolder> mAdapter = new RecyclerView.Adapter<PlaceViewHolder>() {

        @NonNull
        @Override
        public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_place, parent, false);
            return new PlaceViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull PlaceViewHolder holder, int position) {
            holder.bind(mPlaces[position]);
        }

        @Override
        public int getItemCount() {
            return mPlaces.length;
        }
    };

    private static class PlaceViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        //        private TextView tvRating;
        private TextView tvPrice;
        private TextView tvAddress;
        private View vSpacer;

        private static final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Place place = (Place)v.getTag();
                Intent intent = new Intent(v.getContext(), MapsActivity.class);
                intent.putExtra(MapsActivity.EXTRA_PLACE, place);
                v.getContext().startActivity(intent);
            }
        };

        public PlaceViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(mOnClickListener);

            tvName = itemView.findViewById(R.id.text_name);
//            tvRating = itemView.findViewById(R.id.text_rating);
            tvAddress = itemView.findViewById(R.id.text_address);
            tvPrice = itemView.findViewById(R.id.text_price);
            vSpacer = itemView.findViewById(R.id.spacer);
        }

        public void bind(Place place) {
            tvName.setText(place.getName());
//            tvRating.setText(String.valueOf(place.getRating()));
            tvAddress.setText(place.getAddress());
            tvPrice.setVisibility(place.getPrice() == 0 ? View.GONE : View.VISIBLE);
            vSpacer.setVisibility(place.getPrice() == 0 ? View.GONE : View.VISIBLE);
            tvPrice.setText(place.getPriceText());
            itemView.setTag(place);
        }
    }




}
