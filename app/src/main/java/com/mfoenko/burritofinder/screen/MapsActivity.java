package com.mfoenko.burritofinder.screen;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mfoenko.burritofinder.R;
import com.mfoenko.burritofinder.data.Place;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String EXTRA_PLACE = "place";

    private GoogleMap mMap;

    private Place mPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            mPlace = extras.getParcelable(EXTRA_PLACE);
            setTitle(mPlace.getName());

            ((TextView)findViewById(R.id.text_name)).setText(mPlace.getAddress());
            ((TextView)findViewById(R.id.text_details)).setText(mPlace.getPriceText() + " • ");

            if(mMap != null){
                setupMarker();
            }
        }


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we zjust add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //only way to get here is if permission is already given
        mMap.setMyLocationEnabled(true);

        if(mPlace == null){
            return;
        }

        setupMarker();
    }

    private void setupMarker() {
        LatLng placeLoc = new LatLng(mPlace.getLatitude(), mPlace.getLongitude());
        mMap.addMarker(new MarkerOptions().position(placeLoc));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLoc, 15));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
